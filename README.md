A simple DHT based on Chord. Although the design is based on Chord, it is a simplified version of Chord; without the implementation finger tables, finger-based routing and also does not node leaves/failures.

The main pieces of the implementation are
1) ID space partitioning/re-partitioning, 
2) Ring-based routing, and 
3) Node joins.