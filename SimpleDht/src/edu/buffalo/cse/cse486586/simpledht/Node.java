package edu.buffalo.cse.cse486586.simpledht;

public class Node implements ChordEntry{
	public String id;
	public String port;
	public String hash;
	public Node predecessor = null;
	public Node successor = null;
	
	public Node(String id,String port, String nodeHash){
		this.id = id;
		this.port = port;
		this.hash = nodeHash;
		this.predecessor = null;
		this.successor = null;
	}
	
	public Node getPredecessor(){
		return this.predecessor;
	}
	
	public Node getSuccessor(){
		return this.successor;
	}
	
	public String getHash(){
		return this.hash;
	}

	public void setPredeccesor(Node predecessor){
		this.predecessor = predecessor;
	}
	
	public void setPredecessor(Node successor){
		this.successor = successor;
	}
	@Override
	public int compareTo(ChordEntry another) {
		return this.hash.compareTo(another.getHash());
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(this.id);
		sb.append("||");
		sb.append(this.port);
		sb.append("||");
		sb.append(this.hash);
		return sb.toString();
	}

	public boolean storeData(Data data) {
		// TODO Auto-generated method stub
		if(this.successor == null){
			return true;
		}
		if(this.compareTo(data) > 0 && this.predecessor.compareTo(data) <= 0){
			return true;
		} else if(this.predecessor.compareTo(this) > 0 &&
				((this.compareTo(data) > 0 && this.predecessor.compareTo(data) > 0) ||
				(this.compareTo(data) < 0 && this.predecessor.compareTo(data) < 0))){
			return true;
		}
		return false;
	}
}