package edu.buffalo.cse.cse486586.simpledht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import android.util.Log;

public class ServerTask implements Runnable{

	private static final int SERVER_PORT = 10000;
	@Override
	public void run(){
		// TODO Auto-generated method stub
		ServerSocket serverSocket;
        BufferedReader br = null;
        try {
			serverSocket = new ServerSocket(SERVER_PORT);
            while (true) {
                Socket client = serverSocket.accept();
                InputStream is = client.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
                String line = br.readLine();
                new Thread(new MessageProcessor(line,SimpleDhtProvider.node)).start();
            }
        } catch (IOException ex) {
            Log.v("Server Connection", "Coonection Accept Failed");
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Log.v("Server Connection", "Reader Close Failed");
            }
        }
	}
}