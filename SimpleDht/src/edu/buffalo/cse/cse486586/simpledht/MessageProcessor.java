package edu.buffalo.cse.cse486586.simpledht;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;
import edu.buffalo.cse.cse486586.simpledht.message.DataInsertMessage;
import edu.buffalo.cse.cse486586.simpledht.message.DeleteMessage;
import edu.buffalo.cse.cse486586.simpledht.message.JoinMessage;
import edu.buffalo.cse.cse486586.simpledht.message.Message;
import edu.buffalo.cse.cse486586.simpledht.message.JoinReplyMessage;
import edu.buffalo.cse.cse486586.simpledht.message.MessageFactory;
import edu.buffalo.cse.cse486586.simpledht.message.NodeInsertMessage;
import edu.buffalo.cse.cse486586.simpledht.message.QueryMessage;
import edu.buffalo.cse.cse486586.simpledht.message.SuccessorChangeMessage;

public class MessageProcessor implements Runnable{
	
	String line;	
	
	public MessageProcessor(String line,Node node){
		this.line = line;
	}
	
	@Override
	public void run(){
		Message msg = MessageFactory.getInstance().getMessage(line);
		msg.accept(this);
	}
	
	public void visit(JoinMessage message){
		Node node = message.getNode();
		if(SimpleDhtProvider.node.getPredecessor() == null){
			// No other nodes are there. Add to 5554
			Log.v("Join Message","No other nodes.. Adding to the ring");
			SimpleDhtProvider.node.successor = node;
			SimpleDhtProvider.node.predecessor = node;
			Log.v("Current Structure",SimpleDhtProvider.node.predecessor.id+"||"+SimpleDhtProvider.node.id+"||"+SimpleDhtProvider.node.successor.id);
			Message msg = new JoinReplyMessage(SimpleDhtProvider.node,SimpleDhtProvider.node,node.port);
			new Thread(new ClientTask(msg)).start();
		}else{
			if(SimpleDhtProvider.node.compareTo(node) <= 0 && SimpleDhtProvider.node.predecessor.compareTo(node) > 0){
				Node prev = SimpleDhtProvider.node.predecessor;
				Node replySuccessor = SimpleDhtProvider.node;
				SimpleDhtProvider.node.predecessor = node;
				Log.v("Current Structure",SimpleDhtProvider.node.predecessor.id+"||"+SimpleDhtProvider.node.id+"||"+SimpleDhtProvider.node.successor.id);
				Message msg = new SuccessorChangeMessage(node,prev.port);
				new Thread(new ClientTask(msg)).start();
				Message reply = new JoinReplyMessage(prev,replySuccessor,node.port);
				new Thread(new ClientTask(reply)).start();
			}else if(SimpleDhtProvider.node.predecessor.compareTo(SimpleDhtProvider.node) > 0 &&
					((SimpleDhtProvider.node.compareTo(node) > 0 && SimpleDhtProvider.node.predecessor.compareTo(node) > 0) ||
					(SimpleDhtProvider.node.compareTo(node) < 0 && SimpleDhtProvider.node.predecessor.compareTo(node) < 0))){
				Node prev = SimpleDhtProvider.node.predecessor;
				Node replySuccessor = SimpleDhtProvider.node;
				Log.v("Current Structure",SimpleDhtProvider.node.predecessor.id+"||"+SimpleDhtProvider.node.id+"||"+SimpleDhtProvider.node.successor.id);
				Message msg = new SuccessorChangeMessage(node,prev.port);
				new Thread(new ClientTask(msg)).start();
				Message reply = new JoinReplyMessage(prev,replySuccessor,node.port);
				new Thread(new ClientTask(reply)).start();
				SimpleDhtProvider.node.predecessor = node;

			} else { 
				// Forward to successor
				Message msg = new NodeInsertMessage(node,SimpleDhtProvider.node.successor.port); 
				new Thread(new ClientTask(msg)).start();
				Log.v("Message Processor","Forwarded to"+SimpleDhtProvider.node.successor.port);
			}
		}
	}
	
	public void visit(JoinReplyMessage message){
		Node prevNode = message.getPrev();
		Node nextNode = message.getNext();
		SimpleDhtProvider.node.setPredeccesor(prevNode);
		SimpleDhtProvider.node.setPredecessor(nextNode);
		Log.v("Current Structure",SimpleDhtProvider.node.predecessor.id+"||"+SimpleDhtProvider.node.id+"||"+SimpleDhtProvider.node.successor.id);
	}
	
	public void visit(SuccessorChangeMessage message){
		// Modify the current nodes successor
		Node next = message.getNode();
		SimpleDhtProvider.node.successor = next;
		Log.v("Current Structure",SimpleDhtProvider.node.predecessor.id+"||"+SimpleDhtProvider.node.id+"||"+SimpleDhtProvider.node.successor.id);
	}
	
	public void visit(NodeInsertMessage message){
		Node node = message.getNode();
		if(SimpleDhtProvider.node.compareTo(node) > 0 && SimpleDhtProvider.node.predecessor.compareTo(node) <= 0){
			Node prev = SimpleDhtProvider.node.predecessor;
			Node replySuccessor = SimpleDhtProvider.node;
			Message msg = new SuccessorChangeMessage(node,prev.port);
			new Thread(new ClientTask(msg)).start();
			Message reply = new JoinReplyMessage(prev,replySuccessor,node.port);
			new Thread(new ClientTask(reply)).start();
			SimpleDhtProvider.node.predecessor = node;
			Log.v("Current Structure",SimpleDhtProvider.node.predecessor.id+"||"+SimpleDhtProvider.node.id+"||"+SimpleDhtProvider.node.successor.id);
		}else if(SimpleDhtProvider.node.predecessor.compareTo(SimpleDhtProvider.node) > 0 &&
				((SimpleDhtProvider.node.compareTo(node) > 0 && SimpleDhtProvider.node.predecessor.compareTo(node) > 0) ||
				(SimpleDhtProvider.node.compareTo(node) < 0 && SimpleDhtProvider.node.predecessor.compareTo(node) < 0))){
			Node prev = SimpleDhtProvider.node.predecessor;
			Node replySuccessor = SimpleDhtProvider.node;
			SimpleDhtProvider.node.predecessor = node;
			Log.v("Current Structure",SimpleDhtProvider.node.predecessor.id+"||"+SimpleDhtProvider.node.id+"||"+SimpleDhtProvider.node.successor.id);
			Message msg = new SuccessorChangeMessage(node,prev.port);
			new Thread(new ClientTask(msg)).start();
			Message reply = new JoinReplyMessage(prev,replySuccessor,node.port);
			new Thread(new ClientTask(reply)).start();
		}else { 
			Message msg = new NodeInsertMessage(node,SimpleDhtProvider.node.successor.port); 
			new Thread(new ClientTask(msg)).start();
		}
	}
	
	public void visit(DataInsertMessage message){
		Data data = message.getData();
		if(SimpleDhtProvider.node.storeData(data)){
	    	ContentValues cv = new ContentValues();
	    	cv.put(SimpleDhtProvider.KEY_FIELD, data.key);
	    	cv.put(SimpleDhtProvider.VALUE_FIELD, data.value);
			SimpleDhtProvider.context.getContentResolver().insert(SimpleDhtProvider.contentUri, cv);
		}else{
			Message msg = new DataInsertMessage(data,SimpleDhtProvider.node.successor.port);
			new Thread(new ClientTask(msg)).start();
		}
	}
	
	public void visit(QueryMessage message){
		// Retrieve Message and send it to successor
		// Generate reply with identifier;
		if (message.getInitiator().equals(SimpleDhtProvider.node.port)) {
			Log.v("Query Reply",message.getData().toString());
			synchronized (SimpleDhtProvider.waitObj) {
				SimpleDhtProvider.cs.setCursor(message.getMatrixCursor());
				SimpleDhtProvider.waitObj.notify();
			}
			
		} else {
			Data data = message.getData();
			if (data.key.equalsIgnoreCase("*")) {
				Cursor cursor = SimpleDhtProvider.context.getContentResolver()
						.query(SimpleDhtProvider.contentUri, null, "**",
								null, null);
				Message msg = new QueryMessage(message.getData(),
						SimpleDhtProvider.node.successor.port,
						message.getInitiator(),message.getMatrixCursor(),cursor);
				new Thread(new ClientTask(msg)).start();
			} else {
				if (SimpleDhtProvider.node.storeData(data)) {
					Cursor cursor = SimpleDhtProvider.context
							.getContentResolver().query(
									SimpleDhtProvider.contentUri, null,
									data.key, null, null);
					Message msg = new QueryMessage(message.getData(),
							message.getInitiator(),
							message.getInitiator(),message.getMatrixCursor(),cursor);
					new Thread(new ClientTask(msg)).start();
					// Send reply to initiator
				} else {
					Message msg = new QueryMessage(message.getData(),
							SimpleDhtProvider.node.successor.port,
							message.getInitiator());
					new Thread(new ClientTask(msg)).start();
				}
			}
		}
	}
	
	public void visit(DeleteMessage message){
		Data data = message.getData();
		if(data.key.equalsIgnoreCase("*")){
			SimpleDhtProvider.context.getContentResolver().delete(SimpleDhtProvider.contentUri, "**", null);
			Message msg = new DeleteMessage(data,SimpleDhtProvider.node.successor.port,message.initiator);
			new Thread(new ClientTask(msg)).start();						
		}else if(SimpleDhtProvider.node.storeData(data)){
			SimpleDhtProvider.context.getContentResolver().delete(SimpleDhtProvider.contentUri, data.key, null);
		}else{
			Message msg = new DeleteMessage(data,SimpleDhtProvider.node.successor.port,message.initiator);
			new Thread(new ClientTask(msg)).start();			
		}
	}
}
