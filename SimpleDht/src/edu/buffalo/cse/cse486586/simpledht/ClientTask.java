package edu.buffalo.cse.cse486586.simpledht;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;

import edu.buffalo.cse.cse486586.simpledht.message.Message;

public class ClientTask implements Runnable{

	Message message;
	public ClientTask(Message message){
		this.message = message;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
        String port = message.getPortNumber();
        Socket socket;
		try {
			socket = new Socket(InetAddress.getByAddress(new byte[] {
			        10, 0, 2, 2
			}), Integer.parseInt(port));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.write(message.toString());
            out.flush();
            out.close();

            socket.close();

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}    	
}