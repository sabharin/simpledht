package edu.buffalo.cse.cse486586.simpledht;

import android.database.Cursor;
import android.text.GetChars;
import android.util.Log;

public class CursorBuilder {

	private Cursor cursor;
	
	public CursorBuilder(){
		
	}
	
	public void setCursor(Cursor cursor){
		this.cursor = cursor;
	}
	
	public Cursor getCursor() {
		try {
			Log.v("CursorBuilder", "Locked with waitObj");
			SimpleDhtProvider.waitObj.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cursor;
	}
}
