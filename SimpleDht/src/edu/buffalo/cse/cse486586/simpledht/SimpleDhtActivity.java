package edu.buffalo.cse.cse486586.simpledht;

import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class SimpleDhtActivity extends Activity {

	String myPort;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_dht_main);
        TextView tv = (TextView) findViewById(R.id.textView1);

        tv.setText(myPort);
        tv.setMovementMethod(new ScrollingMovementMethod());
        findViewById(R.id.button3).setOnClickListener(
                new OnTestClickListener(tv, getContentResolver()));
        
        findViewById(R.id.button1).setOnClickListener(new OnLDumpClickListener());
        findViewById(R.id.button2).setOnClickListener(new OnGDumpClickListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_simple_dht_main, menu);
        return true;
    }
    

	public class OnGDumpClickListener implements OnClickListener {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Log.v("Dump Click", "Global Dump clicked");
			TextView textView = (TextView) findViewById(R.id.textView1);
			Cursor cursor = SimpleDhtProvider.context.getContentResolver()
					.query(SimpleDhtProvider.contentUri, null, "*", null, null);
			if (cursor != null) {
				cursor.moveToFirst();
			}

			int keyindex = cursor.getColumnIndex(SimpleDhtProvider.KEY_FIELD);
			int valueindex = cursor
					.getColumnIndex(SimpleDhtProvider.VALUE_FIELD);

        	for(int i=0;i<cursor.getCount();i++){
    			String key = cursor.getString(keyindex);
    			String value = cursor.getString(valueindex);
    			textView.append(key+"||"+value+"\t\n");
    			cursor.moveToNext();
    		}

			cursor.close();
		}

	}
    
    public class OnLDumpClickListener implements OnClickListener {

    	@Override
    	public void onClick(View arg0) {
    		// TODO Auto-generated method stub
    		Log.v("Dump Click","Local Dump Clicked");
        	TextView textView = (TextView) findViewById(R.id.textView1);
        	Cursor cursor = SimpleDhtProvider.context.getContentResolver().query(SimpleDhtProvider.contentUri, null, "@", null, null);
        	if(cursor != null){
        		cursor.moveToFirst();
        	}

    		int keyindex = cursor.getColumnIndex(SimpleDhtProvider.KEY_FIELD);
    		int valueindex = cursor.getColumnIndex(SimpleDhtProvider.VALUE_FIELD);

        	for(int i=0;i<cursor.getCount();i++){
    			String key = cursor.getString(keyindex);
    			String value = cursor.getString(valueindex);
    			textView.append(key+"||"+value+"\t\n");
    			cursor.moveToNext();
    		}

        	cursor.close();
    	}
    }
}