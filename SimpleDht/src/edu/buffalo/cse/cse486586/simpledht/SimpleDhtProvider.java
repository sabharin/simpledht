package edu.buffalo.cse.cse486586.simpledht;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

import edu.buffalo.cse.cse486586.simpledht.message.*;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;

public class SimpleDhtProvider extends ContentProvider {

	public static final Uri contentUri = Uri
			.parse("content://edu.buffalo.cse.cse486586.simpledht.provider");

	private MessengerDBHelper dbHelper;

	static final String KEY_FIELD = "key";
	static final String VALUE_FIELD = "value";

	private static final String DBNAME = "SIMPLE_DHT";

	private SQLiteDatabase sqliteDatabase = null;

	static String waitObj = new String("waitObj");

	private String portStr;

	static CursorBuilder cs = new CursorBuilder();

	private String myPort = null;

	public static Node node = null;

	static Context context = null;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {

		StringBuilder conditionSb = new StringBuilder(
				MessengerDBHelper.KEY_COLUMN);
		conditionSb.append("=?");

		String hash = null;
		Data data = null;
		try {
			hash = genHash(selection);
			data = new Data(selection, "", hash);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String args[] = new String[1];
		if (selection.equals("@")) {

			args[0] = "*";
			sqliteDatabase = dbHelper.getWritableDatabase();
			sqliteDatabase.delete(MessengerDBHelper.TABLENAME,
					null, null);
		} else if (selection.equals("*")) {

			args[0] = "*";
			sqliteDatabase = dbHelper.getWritableDatabase();
			sqliteDatabase.delete(MessengerDBHelper.TABLENAME,
					null, null);
			if (node.successor != null) {
				Message msg = new DeleteMessage(data, node.successor.port,
						node.port);
				new Thread(new ClientTask(msg)).start();
			}
		} else if (selection.equals("**")) {
			args[0] = "*";
			sqliteDatabase = dbHelper.getWritableDatabase();
			sqliteDatabase.delete(MessengerDBHelper.TABLENAME,
					null, null);

		} else {
			args[0] = selection;
			if (node.storeData(data)) {
				// Log.v("Data Deletion",data.key + "||" +
				// SimpleDhtProvider.node.getHash() + "||" + data.getHash() +
				// "||" + SimpleDhtProvider.node.predecessor.getHash());
				sqliteDatabase = dbHelper.getWritableDatabase();
				sqliteDatabase.delete(MessengerDBHelper.TABLENAME,
						conditionSb.toString(), args);
				// sqliteDatabase.close();
			} else {
				Message msg = new DeleteMessage(data, node.successor.port,
						node.port);
				new Thread(new ClientTask(msg)).start();
			}
		}
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// You do not need to implement this.
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		String key = values.get(KEY_FIELD).toString();
		String value = values.get(VALUE_FIELD).toString();
		String hash = null;
		Data data = null;
		try {
			hash = genHash(key);
			data = new Data(key, value, hash);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (data != null) {
			if (node.storeData(data)) {
				sqliteDatabase = dbHelper.getWritableDatabase();
				Log.v("Query Insert", values.toString());
				sqliteDatabase
						.insert(MessengerDBHelper.TABLENAME, null, values);
			} else {
				Message msg = new DataInsertMessage(data, node.successor.port);
				new Thread(new ClientTask(msg)).start();
			}
		}

		return uri;
	}

	@Override
	public boolean onCreate() {
		// If you need to perform any one-time initialization task, please do it
		// here.
		Log.v("OnCreate", "OnCreate in Provider");
		context = getContext();
		TelephonyManager tel = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		this.portStr = tel.getLine1Number().substring(
				tel.getLine1Number().length() - 4);
		this.myPort = String.valueOf((Integer.parseInt(portStr) * 2));
		dbHelper = new MessengerDBHelper(getContext(), DBNAME, null, 1);

		new Thread(new ServerTask()).start();

		try {
			String nodeHash = genHash(portStr);
			SimpleDhtProvider.node = new Node(portStr, myPort, nodeHash);

			// Send a join Message
			if (!portStr.equalsIgnoreCase("5554")) {
				Message joinMessage = new JoinMessage(SimpleDhtProvider.node);
				new Thread(new ClientTask(joinMessage)).start();
				Log.v("Join Message", "Sent Join Message"
						+ SimpleDhtProvider.node.port);
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		sqliteDatabase = dbHelper.getReadableDatabase();
		StringBuilder conditionSb = new StringBuilder(
				MessengerDBHelper.KEY_COLUMN);
		conditionSb.append("=?");
		String args[] = new String[1];
		args[0] = selection;

		String hash = null;
		Data data = null;
		try {
			hash = genHash(selection);
			data = new Data(selection, "", hash);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Cursor cursor = null;

		if (selection.equals("@")) {
			sqliteDatabase = dbHelper.getReadableDatabase();
			cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME,
					projection, null, null, null, null, null);
			Log.v("Get Count of cursor Local Dump",cursor.getCount()+"");
		} else if (selection.equals("*")) {
			sqliteDatabase = dbHelper.getReadableDatabase();
			cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME,
					projection, null, null, null, null, null);
			Log.v("Local * query","Count is "+ cursor.getCount());
			if (node.successor != null) {
				Log.v("Query Check","We have successor");
				synchronized (SimpleDhtProvider.waitObj) {
					Message msg = new QueryMessage(data,
							SimpleDhtProvider.node.successor.port,
							SimpleDhtProvider.node.port, null, cursor);
					Log.v("Local Query",msg.toString());
					new Thread(new ClientTask(msg)).start();
					cursor = cs.getCursor();
				}
			}
//			Log.v("Get Count of cursor",cursor.getCount()+"");
		} else if (selection.equals("**")) {
			args[0] = "*";
			sqliteDatabase = dbHelper.getReadableDatabase();
			cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME,
					projection, null, null, null, null, null);
		} else {
			args[0] = selection;
			if (node.storeData(data)) {
				sqliteDatabase = dbHelper.getReadableDatabase();
				cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME,
						projection, conditionSb.toString(), args, null, null,
						null);
			} else {
				synchronized (SimpleDhtProvider.waitObj) {

					Message msg = new QueryMessage(data,
							SimpleDhtProvider.node.successor.port,
							SimpleDhtProvider.node.port);
					new Thread(new ClientTask(msg)).start();
					cursor = cs.getCursor();
				}
			}
			//Log.v("Get Count of cursor",cursor.getCount()+"");
		}

		if(cursor != null){
			cursor.moveToFirst();
		}
		int keyindex = cursor.getColumnIndex(SimpleDhtProvider.KEY_FIELD);
		int valueindex = cursor
				.getColumnIndex(SimpleDhtProvider.VALUE_FIELD);
		StringBuilder sb = new StringBuilder();
    	for(int i=0;i<cursor.getCount();i++){
			String key = cursor.getString(keyindex);
			String value = cursor.getString(valueindex);
			sb.append(key);
			sb.append("||");
			sb.append(value);
			sb.append("\t\n");
			cursor.moveToNext();
		}
		
		Log.v("Query", sb.toString());
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// You do not need to implement this.
		return 0;
	}

	private String genHash(String input) throws NoSuchAlgorithmException {
		MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
		byte[] sha1Hash = sha1.digest(input.getBytes());
		Formatter formatter = new Formatter();
		for (byte b : sha1Hash) {
			formatter.format("%02x", b);
		}
		return formatter.toString();
	}
}
