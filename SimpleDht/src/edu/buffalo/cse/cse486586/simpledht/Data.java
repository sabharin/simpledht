package edu.buffalo.cse.cse486586.simpledht;

public class Data implements ChordEntry{
	String key;
	String value;
	String hash;

	public Data(String key,String value,String hash){
		this.key = key;
		this.value = value;
		this.hash = hash;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(key);
		sb.append("||");
		sb.append(value);
		sb.append("||");
		sb.append(hash);
		return sb.toString();
	}

	@Override
	public int compareTo(ChordEntry another) {
		return this.hash.compareTo(another.getHash());
	}

	@Override
	public String getHash() {
		// TODO Auto-generated method stub
		return this.hash;
	}
	
}
