package edu.buffalo.cse.cse486586.simpledht.message;

import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;
import edu.buffalo.cse.cse486586.simpledht.Node;

public class SuccessorChangeMessage implements Message{

	private Node node;
	private String portStr;
	
	public SuccessorChangeMessage(String line) {
		// TODO Auto-generated constructor stub
		String[] components = line.split("\\|\\|");
		String nodeId = components[1];
		String nodePort = components[2];
		String nodeHash = components[3];
		this.node = new Node(nodeId,nodePort,nodeHash);
	}
	
	public SuccessorChangeMessage(Node node,String portStr){
		this.node = node;
		this.portStr = portStr;
	}

	@Override
	public String getPortNumber() {
		// TODO Auto-generated method stub
		return portStr;
	}
	
	public Node getNode() {
		return node;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(5);
		sb.append("||");
		sb.append(node.toString());
		return sb.toString();
	}

	@Override
	public void accept(MessageProcessor msgProcessor) {
		// TODO Auto-generated method stub
		msgProcessor.visit(this);
	}

}
