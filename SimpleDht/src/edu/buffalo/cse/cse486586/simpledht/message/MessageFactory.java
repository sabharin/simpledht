package edu.buffalo.cse.cse486586.simpledht.message;

public class MessageFactory {

	private static MessageFactory instance = null;
	
	private MessageFactory(){
		
	}

	public static MessageFactory getInstance(){
		if(instance == null){
			synchronized(MessageFactory.class){
				if(instance == null){
					instance = new MessageFactory();
				}
			}
		}
		return instance;
	}
	
	public Message getMessage(String line){
        String[] msgComponents = line.split("\\|\\|");
        if(msgComponents[0].equalsIgnoreCase("0")){
			return new JoinMessage(line);
		}else if(msgComponents[0].equalsIgnoreCase("1")){
			return new NodeInsertMessage(line);
		}else if(msgComponents[0].equalsIgnoreCase("2")){
			return new DataInsertMessage(line);
		}else if(msgComponents[0].equalsIgnoreCase("3")){
			return new QueryMessage(line);
		}else if(msgComponents[0].equalsIgnoreCase("4")){
			return new DeleteMessage(line);
		} else if(msgComponents[0].equalsIgnoreCase("5")){
			return new SuccessorChangeMessage(line);
		} else if(msgComponents[0].equalsIgnoreCase("6")){
			return new JoinReplyMessage(line);
		}
		
		return null;
	}
}
