package edu.buffalo.cse.cse486586.simpledht.message;

import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;
import edu.buffalo.cse.cse486586.simpledht.Node;

public class JoinMessage implements Message{

	Node node;
	String sendPort = new String("11108");
	
	public JoinMessage(Node node) {
		this.node = node;
		this.sendPort = new String("11108");
	}
	
	public JoinMessage(String message) {
		String[] components = message.split("\\|\\|");
		String id = components[1];
		String port = components[2];
		String hashValue = components[3];
		node = new Node(id,port,hashValue);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(0);
		sb.append("||");
		sb.append(node.toString());
		return sb.toString();
	}
	
	@Override
	public String getPortNumber() {
		return sendPort;
	}

	@Override
	public void accept(MessageProcessor msgProcessor) {
		msgProcessor.visit(this);
	}

	public Node getNode() {
		return this.node;
	}
}
