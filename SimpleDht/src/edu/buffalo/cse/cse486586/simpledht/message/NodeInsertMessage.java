package edu.buffalo.cse.cse486586.simpledht.message;

import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;
import edu.buffalo.cse.cse486586.simpledht.Node;

public class NodeInsertMessage implements Message{

	String portToJoin ;
	Node node;
	public NodeInsertMessage(String line) {
		// TODO Auto-generated constructor stub
		String[] components = line.split("\\|\\|");
		String id = components[1];
		String port = components[2];
		String hash = components[3];
		this.node = new Node(id,port,hash);
	}

	public NodeInsertMessage(Node node, String port) {
		// TODO Auto-generated constructor stub
		this.node = node;
		this.portToJoin = port;
	}

	@Override
	public String getPortNumber() {
		// TODO Auto-generated method stub
		return this.portToJoin;
	}
	
	public Node getNode(){
		return this.node;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(1);
		sb.append("||");
		sb.append(node.toString());
		return sb.toString();
	}

	@Override
	public void accept(MessageProcessor msgProcessor) {
		// TODO Auto-generated method stub
		msgProcessor.visit(this);
	}


}
