package edu.buffalo.cse.cse486586.simpledht.message;

import edu.buffalo.cse.cse486586.simpledht.Data;
import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;

public class DeleteMessage implements Message{

	public String initiator;
	private String portNumber;
	private Data data;
	
	public DeleteMessage(String line){
		String[] components = line.split("\\|\\|");
		this.initiator = components[1];
		String key = components[2];
		String value = components[3];
		String hash = components[4];
		this.data = new Data(key, value, hash); 
	}
	
	public DeleteMessage(Data data, String port, String initiator) {
		// TODO Auto-generated constructor stub
		this.initiator = initiator;
		this.portNumber = port;
		this.data = data;
	}

	@Override
	public String getPortNumber() {
		// TODO Auto-generated method stub
		return portNumber;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(4);
		sb.append("||");
		sb.append(initiator);
		sb.append("||");
		sb.append(data.toString());
		return sb.toString();
	}

	@Override
	public void accept(MessageProcessor msgProcessor) {
		// TODO Auto-generated method stub
		msgProcessor.visit(this);
	}

	public Data getData() {
		// TODO Auto-generated method stub
		return data;
	}


}
