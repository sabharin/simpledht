package edu.buffalo.cse.cse486586.simpledht.message;

import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;

public interface Message {

	public String getPortNumber();
	public void accept(MessageProcessor msgProcessor);

}
