package edu.buffalo.cse.cse486586.simpledht.message;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.util.Log;
import edu.buffalo.cse.cse486586.simpledht.Data;
import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;
import edu.buffalo.cse.cse486586.simpledht.SimpleDhtProvider;

public class QueryMessage implements Message{

	public Data data;
	public String port;
	public String initiator;
	private MatrixCursor mx = null;
	private boolean reply = false;
	
	protected static final String KEY_COLUMN = "key";

	protected static final String VALUE_COLUMN = "value";
	
	public QueryMessage(String line) {
		// TODO Auto-generated constructor stub
		String[] components = line.split("\\|\\|");
		this.initiator = components[1];
		String key = components[2];
		String value = components[3];
		String hash = components[4];
		boolean reply = Boolean.parseBoolean(components[5]);
		this.data = new Data(key, value, hash); 
		if(reply){
			String[] columnNames = new String[2];
			columnNames[0] = KEY_COLUMN;
			columnNames[1] = VALUE_COLUMN;
			this.mx = new MatrixCursor(columnNames);
			for(int i=6;i<components.length;i+=2){
				String[] columnValues = new String[2];
				columnValues[0] = components[i];
				columnValues[1] = components[i+1];
				mx.addRow(columnValues);
			}
		}
	}

	public QueryMessage(Data data, String port, String senderPort) {
		// TODO Auto-generated constructor stub
		this.data = data;
		this.port = port;
		this.initiator = senderPort;
	}

	public QueryMessage(Data data, String port, String initiator,MatrixCursor matrixCursor,
			Cursor cursor) {
		this.data = data;
		this.port = port;
		this.initiator = initiator;
		this.mx = matrixCursor;
		if(mx == null){
			String[] columnNames = new String[2];
			columnNames[0] = KEY_COLUMN;
			columnNames[1] = VALUE_COLUMN;
			mx = new MatrixCursor(columnNames);
		}else{
			this.reply = true;
		}
		
		if(cursor!=null){
			this.reply = true;
			cursor.moveToFirst();
		}

		for(int i = 0; i < cursor.getCount(); i++){
			int keyIndex = cursor.getColumnIndex(KEY_COLUMN);
			int valIndex = cursor.getColumnIndex(VALUE_COLUMN);
			if (keyIndex == -1 || valIndex == -1) {
				continue;
			}
			String key = cursor.getString(keyIndex);
			String val = cursor.getString(valIndex);
			String[] columnValues = new String[2];
			columnValues[0] = key;
			columnValues[1] = val;
			mx.addRow(columnValues);
			cursor.moveToNext();
		}
	}

	@Override
	public String getPortNumber() {
		// TODO Auto-generated method stub
		return port;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(3);
		sb.append("||");
		sb.append(initiator);
		sb.append("||");
		sb.append(data.toString());
		sb.append("||");
		sb.append(reply);
		if(mx != null){
			sb.append("||");
			mx.moveToFirst();
			for(int i = 0; i < mx.getCount(); i++){
				int keyIndex = mx.getColumnIndex(KEY_COLUMN);
				int valIndex = mx.getColumnIndex(VALUE_COLUMN);
				String key = mx.getString(keyIndex);
				String val = mx.getString(valIndex);
				sb.append(key);
				sb.append("||");
				sb.append(val);
				sb.append("||");
				mx.moveToNext();
			}
		}
		return sb.toString();
	}

	@Override
	public void accept(MessageProcessor msgProcessor) {
		// TODO Auto-generated method stub
		msgProcessor.visit(this);
	}

	public Data getData() {
		// TODO Auto-generated method stub
		return this.data;
	}

	public String getInitiator() {
		// TODO Auto-generated method stub
		return this.initiator;
	}

	public MatrixCursor getMatrixCursor() {
		// TODO Auto-generated method stub
		return this.mx;
	}
}
