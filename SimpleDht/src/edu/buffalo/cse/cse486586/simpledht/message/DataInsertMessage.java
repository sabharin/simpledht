package edu.buffalo.cse.cse486586.simpledht.message;

import edu.buffalo.cse.cse486586.simpledht.Data;
import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;

public class DataInsertMessage implements Message{

	public Data data;
	public String portToSend;
	
	public DataInsertMessage(String line) {
		// TODO Auto-generated constructor stub
		String[] components = line.split("\\|\\|");
		String key = components[1];
		String value = components[2];
		String hash = components[3];
		data = new Data(key,value,hash);
	}

	public DataInsertMessage(Data data, String port) {
		// TODO Auto-generated constructor stub
		this.data = data;
		this.portToSend = port;
	}

	@Override
	public String getPortNumber() {
		// TODO Auto-generated method stub
		return portToSend;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(2);
		sb.append("||");
		sb.append(data.toString());
		return sb.toString();
	}

	@Override
	public void accept(MessageProcessor msgProcessor) {
		// TODO Auto-generated method stub
		msgProcessor.visit(this);
	}

	public Data getData() {
		// TODO Auto-generated method stub
		return data;
	}

}
