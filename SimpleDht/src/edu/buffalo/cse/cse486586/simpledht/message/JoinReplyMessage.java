package edu.buffalo.cse.cse486586.simpledht.message;

import edu.buffalo.cse.cse486586.simpledht.MessageProcessor;
import edu.buffalo.cse.cse486586.simpledht.Node;

public class JoinReplyMessage implements Message {

	private Node next;
	private Node prev;
	private String port;
	
	public JoinReplyMessage(Node prev, Node next, String port) {
		this.prev = prev;
		this.next = next;
		this.port = port;
	}

	public JoinReplyMessage(String line) {
		String[] components = line.split("\\|\\|");
		String prevNodeId = components[1];
		String prevNodePort = components[2];
		String prevNodeHash = components[3];
		this.prev = new Node(prevNodeId,prevNodePort,prevNodeHash);
		String nextNodeId = components[4];
		String nextNodePort = components[5];
		String nextNodeHash = components[6];
		this.next = new Node(nextNodeId,nextNodePort,nextNodeHash);
	}

	@Override
	public String getPortNumber() {
		return port;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(6);
		sb.append("||");
		sb.append(prev.toString());
		sb.append("||");
		sb.append(next.toString());
		return sb.toString();
	}
	
	public Node getPrev(){
		return this.prev;
	}
	
	public Node getNext(){
		return this.next;
	}

	@Override
	public void accept(MessageProcessor msgProcessor) {
		// TODO Auto-generated method stub
		msgProcessor.visit(this);
	}

}
